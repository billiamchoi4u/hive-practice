DROP TABLE ratings;
CREATE TABLE IF NOT EXISTS ratings(
  userid INT,
  movieid INT,
  rating DOUBLE,
  ts INT
)
row format delimited fields terminated BY ',' lines terminated BY '\n'
tblproperties("skip.header.line.count"="1");
LOAD DATA LOCAL INPATH './ratings.csv' OVERWRITE INTO TABLE ratings;

SELECT * FROM ratings LIMIT 30;