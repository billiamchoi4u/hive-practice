DROP TABLE ratings2;
CREATE EXTERNAL TABLE IF NOT EXISTS ratings2(
  userid INT,
  movieid INT,
  rating DOUBLE,
  ts INT
)
row format delimited fields terminated BY ',' lines terminated BY '\n'
LOCATION '/user/maria_dev/ml-latest/ratings'
tblproperties("skip.header.line.count"="1");

DROP TABLE movie2;
CREATE EXTERNAL TABLE IF NOT EXISTS movie2(
  movieid INT,
  title STRING,
  genre STRING
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
LOCATION '/user/maria_dev/ml-latest/movies'
tblproperties("skip.header.line.count"="1");